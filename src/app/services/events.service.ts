import { Injectable } from '@angular/core'
import { EventEmitter } from 'rxjs-event-emitter'

export const emitter = new EventEmitter

export const applicationOpened = 'application-opened'

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor() { }
}
