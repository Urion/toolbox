import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutePassingComponent } from './route-passing.component';

describe('RoutePassingComponent', () => {
  let component: RoutePassingComponent;
  let fixture: ComponentFixture<RoutePassingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutePassingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutePassingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
