import { Component, OnInit, AfterContentInit } from '@angular/core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-route-passing',
  templateUrl: './route-passing.component.html',
  styleUrls: ['./route-passing.component.scss']
})
export class RoutePassingComponent implements OnInit, AfterContentInit {

  constructor(private route: Router) { }

  ngAfterContentInit() {
    this.route.navigate(['/application'])
  }

  ngOnInit() {

  }

}
