import { Component, OnInit, ChangeDetectorRef, AfterContentInit, OnDestroy } from '@angular/core'
import { emitter, applicationOpened } from '../../services/events.service'

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})

export class ApplicationComponent implements OnInit, AfterContentInit, OnDestroy {

  private urlSub

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngAfterContentInit(): void {
    this.urlSub = emitter.on(applicationOpened, (url) => {
      const view = document.getElementById('view')
      console.log(`Open url ${url}`)
      view.setAttribute('src', url)
      this.cdr.detectChanges()
    })

    emitter.emit('getApp', '')
  }

  ngOnDestroy(): void {
    // Called once, before the instance is destroyed.
    // Add 'implements OnDestroy' to the class.
    this.urlSub.unsubscribe()
  }

  ngOnInit() {
    const view = document.getElementById('view')
    view.removeAttribute('src')
    console.log(window.location)
  }

}
