import { Component, OnInit } from '@angular/core'
import { emitter, applicationOpened } from '../../services/events.service'
import { Router } from '@angular/router'

const open = require('open')
const loadJsonFile = require('load-json-file')

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  public options
  public url
  public sub
  public applications

  constructor( private router: Router) {
    this.options = {
      value: {
        bottom: 0,
        fixed: false,
        top: 0
      }
    }

    this.sub = emitter.on('getApp', (data) => {
      emitter.emit(applicationOpened, this.url)
    })
  }

  openApplication(url: String) {
    this.url = url
    this.router.navigate(['/passing'])
    console.log('BOH')
  }

  loadApplications() {
    loadJsonFile('src/app/model/webapp.json')
    .then(json => {
      console.log(json)
      this.applications = json
      console.log(this.applications)
    })
  }

  ngOnInit() {
    this.loadApplications()
  }

}
