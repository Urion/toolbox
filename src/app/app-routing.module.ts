import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SettingsComponent } from './components/settings/settings.component'
import { ApplicationComponent } from './components/application/application.component'
import { RoutePassingComponent } from './components/route-passing/route-passing.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'settings',
    pathMatch: 'full'
  }, {
    path: 'settings',
    component: SettingsComponent
  }, {
    path: 'application/:appName',
    component: ApplicationComponent
  }, {
    path: 'application',
    component: ApplicationComponent
  }, {
    path: 'passing',
    component: RoutePassingComponent
  }
]

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
